# Segment Assignment #

### What assignment am I doing? ###

Option 1

### How to run ###

From the main segment-challenge folder in terminal, run
`python -c "import segment_challenge; print segment_challenge.find_available_sdr('<FILE_LOCATION>', '<SEARCH_DATETIME>')"

This is assuming you want to print out the results of the function to see.

### Assumptions ###

<FILE_LOCATION> will be an absolute path.

<SEARCH_DATETIME> will be in the format ['%a %I:%M %p'](http://strftime.org/). IE, 'Thu 11:00 am'. Note: for hours that are < 10, no leading 0 is needed. IE, both 'Thu 9:00 am' and 'Thu 09:00 am' are fine.

The python version I am using is 2.7.

I'm assuming if there are no matches, then you want an empty list returned.

For CSV file passed in, I'm assuming the structure will be the same as the file, where the first part is the sdr email, and the second part is an inner csv that is in quotes if there is more than input interval.

I'm also assuming that there won't be some giant CSV that's passed in where performance becomes a major issue.