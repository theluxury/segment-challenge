from collections import defaultdict
from datetime import time, datetime

# list of allowable days in order
DAYS = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri']
# python has weird thing where input is slightly different from output. This is to make output identical
# to input as it is given. as in, no leading 0 for hour. ie, 7:00 am instead of 07:00 am
INPUT_TIME_FORMAT = '%I:%M %p'
OUTPUT_TIME_FORMAT = '%-I:%M %p'


class TimeOfWeek(object):
    def __init__(self, day_of_week, search_time):
        assert day_of_week in DAYS, '{0} is not a proper day of the week'.format(day_of_week)
        assert isinstance(search_time, time), '{0} is not a proper time'.format(search_time)
        self._day_of_week = day_of_week
        # I believe time is immutable so don't need defensive copies.
        self._search_time = search_time

    @property
    def day_of_week(self):
        return self._day_of_week

    @property
    def search_time(self):
        return self._search_time

    # Takes something like 'Thu 11:00 am' and returns a TimeOnWeek object
    @staticmethod
    def from_string(search_datetime):
        return TimeOfWeek(search_datetime.split(' ', 1)[0], TimeOfWeek.parse_time(search_datetime.split(' ', 1)[1]))

    def __str__(self):
        return '{0} {1}'.format(self.day_of_week,
                                # lower case since input is.
                                str.lower(self.search_time.strftime(OUTPUT_TIME_FORMAT)))

    @staticmethod
    def parse_time(time_input):
        return datetime.strptime(time_input, INPUT_TIME_FORMAT).time()


class TimeOfWeekInterval(object):
    def __init__(self, day_of_week, start_time, end_time):
        assert day_of_week in DAYS, '{0} is not a proper day of the week'.format(day_of_week)
        assert isinstance(start_time, time), '{0} is not a proper time'.format(start_time)
        assert isinstance(end_time, time), '{0} is not a proper time'.format(end_time)
        assert start_time.__le__(end_time)
        self._day_of_week = day_of_week
        # I believe time is immutable so don't need defensive copies.
        self._start_time = start_time
        self._end_time = end_time

    @property
    def day_of_week(self):
        return self._day_of_week

    @property
    def start_time(self):
        return self._start_time

    @property
    def end_time(self):
        return self._end_time

    # Takes something like ('Mon', '11:30 am - 1:30 pm') and returns a TimeOfWeekInterval object
    @staticmethod
    def from_string(day_of_week, time_interval):
        time_start, time_end = time_interval.split(' - ')
        return TimeOfWeekInterval(day_of_week,
                                  TimeOfWeek.parse_time(time_start),
                                  TimeOfWeek.parse_time(time_end))

    # whether this interval contains a TimeOnWeek object.
    def contains_time(self, time_of_week):
        assert isinstance(time_of_week, TimeOfWeek)
        return time_of_week.day_of_week == self.day_of_week \
               and time_of_week.search_time.__ge__(self.start_time) \
               and time_of_week.search_time.__le__(self.end_time)

    def __str__(self):
        return '{0} {1} - {2}'.format(self.day_of_week,
                                      # lower case since input is.
                                      str.lower(self.start_time.strftime(OUTPUT_TIME_FORMAT)),
                                      str.lower(self.end_time.strftime(OUTPUT_TIME_FORMAT)))


# Takes either ddd or ddd-ddd and returns a list of the days in the range.
def __parse_days(days_input):
    days_list = []
    if '-' in days_input:
        day_start, day_end = days_input.split('-')
        for i in range(DAYS.index(day_start), DAYS.index(day_end) + 1):
            days_list.append(DAYS[i])
    else:
        # this instance is just one day
        days_list.append(days_input)
    return days_list


# takes an absolute path and returns a dictionary of {sdr: [avail_time_of_week]}
def __parse_all_intervals(path):
    sdr_dict = defaultdict(list)
    with open(path) as f:
        lines = f.readlines()

    def parse_avail(csv):
        intervals = csv.split(',')
        return_list = []
        for interval in intervals:
            # the times sometimes have leading and ending white spaces, and white spaces around -, so we just remove
            interval = interval.strip()
            avail_days = __parse_days(interval.split(' ', 1)[0])
            time_interval = interval.split(' ', 1)[1]
            return_list.extend(map(lambda day: TimeOfWeekInterval.from_string(day, time_interval), avail_days))
        return return_list

    for line in lines:
        # we remove quotes from the time interval if there are any.
        sdr, avail_interval = line.split(',', 1)[0], line.split(',', 1)[1].replace('"', '')
        sdr_dict[sdr] = parse_avail(avail_interval)
    return sdr_dict


# takes an absolute path for a csv file formatted like the assignment and a search_datetime
# formatted as in the csv and returns a list of emails of users who are available during the time.
def find_available_sdr(csv_filename, search_datetime):
    interval_dict = __parse_all_intervals(csv_filename)
    parsed_search_datetime = TimeOfWeek.from_string(search_datetime)
    avail_sdr = []
    for sdr, avail_times in interval_dict.iteritems():
        if any(avail_time.contains_time(parsed_search_datetime) for avail_time in avail_times):
            avail_sdr.append(sdr)
    return avail_sdr
