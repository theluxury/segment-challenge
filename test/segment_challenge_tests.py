import os
import unittest
from datetime import time, datetime

from segment_challenge import TimeOfWeek, TimeOfWeekInterval, find_available_sdr
from segment_challenge import __parse_all_intervals as _parse_all_intervals
from segment_challenge import __parse_days as _parse_days

CURRENT_DIR = os.path.dirname(__file__)
REL_PATH = "../resources/sdr_availability.csv"
FILE_PATH = os.path.join(CURRENT_DIR, REL_PATH)


class MyTestCase(unittest.TestCase):
    def time_of_week_rejects_bad_values(self):
        self.assertRaises(AssertionError, TimeOfWeek, 'NotDay', datetime.now().time())
        self.assertRaises(AssertionError, TimeOfWeek, 'Mon', datetime.now())

    def time_of_week_interval_rejects_bad_values(self):
        self.assertRaises(AssertionError, TimeOfWeekInterval, 'NotDay', time(hour=12, minute=00),
                          time(hour=12, minute=30))
        self.assertRaises(AssertionError, TimeOfWeekInterval, 'Mon', datetime.now(), time(hour=12, minute=30))
        self.assertRaises(AssertionError, TimeOfWeekInterval, 'Mon', time(hour=12, minute=00), datetime.now())
        # endtime before starttime
        self.assertRaises(AssertionError, TimeOfWeekInterval, 'NotDay', time(hour=12, minute=30),
                          time(hour=12, minute=00))

    def time_of_week_from_string(self):
        self.assertEquals(TimeOfWeek.from_string('Mon 11:00 am').search_time, time(hour=11))

    def time_of_week_interval_from_string(self):
        self.assertEquals(TimeOfWeekInterval.from_string('Mon', '11:00 am - 12:00 pm').start_time, time(hour=11))
        self.assertEquals(TimeOfWeekInterval.from_string('Mon', '11:00 am - 12:00 pm').end_time, time(hour=12))

    def time_of_week_interval_contains_works(self):
        tow = TimeOfWeek('Mon', time(hour=12, minute=00))
        tow2 = TimeOfWeek('Mon', time(hour=12, minute=30))
        other_tow = TimeOfWeek('Mon', time(hour=12, minute=31))
        tow_interval = TimeOfWeekInterval('Mon', time(hour=12, minute=00), time(hour=12, minute=30))
        self.assertTrue(tow_interval.contains_time(tow))
        self.assertTrue(tow_interval.contains_time(tow2))
        self.assertFalse(tow_interval.contains_time(other_tow))

    def parse_days_test(self):
        self.assertEquals(_parse_days('Mon'), ['Mon'])
        self.assertEquals(_parse_days('Mon-Thu'), ['Mon', 'Tue', 'Wed', 'Thu'])

    def parse_time_test(self):
        self.assertEquals(TimeOfWeek.parse_time('11:00 am'), time(hour=11))

    def parse_avail_test(self):
        self.assertEquals(map(lambda x: str(x), _parse_all_intervals(FILE_PATH)['sdr5@segment.com']),
                          ['Fri 11:00 am - 11:30 am'])
        self.assertEquals(map(lambda x: str(x), _parse_all_intervals(FILE_PATH)['sdr7@segment.com']),
                          ['Mon 10:30 am - 11:30 am', 'Tue 10:30 am - 11:30 am', 'Wed 10:30 am - 11:30 am',
                           'Tue 11:30 am - 3:00 pm', 'Fri 10:00 am - 2:00 pm'])

    def find_available_sdr_test(self):
        self.assertEquals(sorted(find_available_sdr(FILE_PATH, 'Tue 11:30 am')),
                          ['sdr10@segment.com', 'sdr1@segment.com', 'sdr3@segment.com', 'sdr6@segment.com',
                           'sdr7@segment.com', 'sdr8@segment.com', 'sdr9@segment.com'])
        self.assertEquals(sorted(find_available_sdr(FILE_PATH, 'Wed 12:00 pm')),
                          ['sdr11@segment.com', 'sdr14@segment.com', 'sdr6@segment.com'])
        self.assertEquals(sorted(find_available_sdr(FILE_PATH, 'Mon 9:00 am')),
                          ['sdr12@segment.com', 'sdr3@segment.com', 'sdr6@segment.com', 'sdr9@segment.com'])
        self.assertEquals(sorted(find_available_sdr(FILE_PATH, 'Fri 11:50 pm')),
                          [])
